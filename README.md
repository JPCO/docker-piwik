# Piwik
---
Made because I needed a Piwik installation for my raspberry.

Took too long to build using the code base from the php7-fpm and piwik combined.
So ended up using debian packages for just about everything.

Using fpm on php7
----

```
FPM Port: 9000
Piwik files are extracted on first run to /var/www/html/piwik
```


Nginx configuration example:
```
server {
    listen 443;

    server_name         mydomain.com;
    index               index.php;

    root                /var/www/html/;
    
    location ~ ^/piwik/.*\.php$ {
        include         fastcgi.conf;
        fastcgi_pass    piwik-fpm:9000;
    }
}

```