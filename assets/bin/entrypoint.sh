#!/bin/bash
set -e

if [ ! -e piwik ]; then
	mkdir piwik
 	tar xf /usr/src/piwik.tar.gz -C piwik
	chown -R www-data .
fi

exec "$@"